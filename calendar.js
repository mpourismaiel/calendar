const $ = require('jquery')
const storage = require('electron-json-storage')
const template = require('lodash').template\

const monthFull = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
const monthAbbr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
const weekdayFull = ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
const weekdaysAbbr = ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri']

function daysInMonth(year, month) {
  return new Date(year, month, 0).getDate();
}

function attachEvents () {
  const events = this.events || {}
  Object.keys(events).forEach(event => {
    const e = event.slice(0, event.indexOf(' '))
    const selector = event.slice(event.indexOf(' ') + 1)

    this.calendar.on(e, selector, events[event].bind(this))
  })
}

const calendar = {
  calendar: $('#calendar'),

  constructor () {
    this.dateChosen = new Date()

    this.year = this.dateChosen.getFullYear()
    this.month = this.dateChosen.getMonth() + 1
    this.day = this.dateChosen.getDate()
    attachEvents.apply(this)

    this.generateCalendar(this.year, this.month)
  },

  generateTitle () {
    return template($('#calendar-title').html())({
      year: this.year,
      month: monthFull[this.month - 1]
    })
  },

  generateWeekdays () {
    const weekdaysIndex = [1, 2, 3, 4, 5, 6, 0]
    return template($('#calendar-weekdays').html())({
      weekdaysIndex,
      weekdaysAbbr
    })
  },

  generateCalendar () {
    let html = ''
    html += this.generateTitle()
    html += this.generateWeekdays()
    html += template($('#calendar-days').html())({
      daysInMonth: daysInMonth(this.year, this.month),
      year: this.year,
      month: this.month
    })

    this.calendar.html('')
    this.calendar.append($(html))
  },

  generateUserEvents (events) {
    return (events.data || []).reduce((html, event) => {
      html += `<span class='event'>${event}</span>`
      return html
    }, '')
  },

  renderEvents ($day) {
    const day = $day.data('day')
    storage.get([this.year, this.month, day].join('/'), (err, data) => {
      if (err) {
        console.error(err)
        debugger
        return
      }

      this.calendar.find('.event-area').remove()
      let html = `
        <div class='event-area' data-day='${day}'>
          <div class='event-creator'>
            <input type='text' placeholder='Event title...'>
          </div>
          <div class='events'>
            ${this.generateUserEvents(data)}
          </div>
        </div>`
      $day.closest('.calendar-week').after($(html))
      this.calendar.find('.calendar-day.selected').removeClass('selected')
      $day.addClass('selected')
    })
  },

  events: {
    'click.day .calendar-day' (e) {
      const $this = $(e.target)
      if ($this.hasClass('day-empty')) return

      if (!$this.hasClass('selected')) {
        this.renderEvents($this)
      } else {
        this.calendar.find('.event-area').remove()
        this.calendar.find('.calendar-day.selected').removeClass('selected')
      }
    },
    'click [data-ui="nextMonth"]' () {
      this.month++
      if (this.month > 12) {
        this.month = 1
        this.year++
      }

      this.generateCalendar()
    },
    'click [data-ui="prevMonth"]' () {
      this.month--
      if (this.month < 1) {
        this.month = 12
        this.year--
      }

      this.generateCalendar()
    },
    'click [data-ui="nextYear"]' () {
      this.year++
      this.generateCalendar()
    },
    'click [data-ui="prevYear"]' () {
      this.year--
      this.generateCalendar()
    },

    'keydown .event-creator input' (e) {
      if (e.which === 13 && $(e.target).val().trim().length > 0) {
        const $target = $(e.target)
        const value = $target.val().trim()
        const day = $target.closest('.event-area').data('day')

        storage.get([this.year, this.month, day].join('/'), (err, event) => {
          const prevData = event.data || []

          prevData.push(value)
          storage.set([this.year, this.month, day].join('/'), { data: prevData })
          this.renderEvents(this.calendar.find(`.calendar-day[data-day="${day}"]`))
          this.calendar.find('.event-creator input').val('').focus()
        })
      }
    }
  }
}

calendar.constructor()
